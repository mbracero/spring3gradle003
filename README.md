## Apuntes

**Spring3 (boot, mvc, SECURITY) + gradle**

http://docs.spring.io/spring-security/site/docs/3.2.7.RELEASE/reference/htmlsingle/

Ejemplo simple de uso de spring security.

Authorization & Authentication (request).

El formulario de login lo crea directamente spring.

Las URL's de login & logout son las siguientes:

> http://localhost:9000/login

> http://localhost:9000/logout



Probar directamente en navegador, existen 3 usuarios con tres roles definidos:

> user/password --> USER

> admin/password --> USER & ADMIN

> dba/password --> DBA & ADMIN



**HTTP & HTTPS**

Indicamos el canal de comunicacion dependiendo de la URL a la que se accede.

(Se ha definido como ejemplo en la practica no se devuelven datos a traves de la URL designada como segura porque hay que definir el metodo criptografico TODO SSL)


**Get currently authenticated user**

See:

> http://localhost:9000/api/items

> http://localhost:9000/api/items2



[Spring MVC Async Integration](http://docs.spring.io/spring-security/site/docs/3.2.7.RELEASE/reference/htmlsingle/#mvc-async)

Por ejemplo para subidas de ficheros:

```
@RequestMapping(method=RequestMethod.POST)
public Callable<String> processUpload(final MultipartFile file) {

  return new Callable<String>() {
    public Object call() throws Exception {
      // ...
      return "someView";
    }
  };
}
```


Las URL's a las que se pueden acceder son las siguientes:

> http://localhost:9000/signup

> http://localhost:9000/about

> http://localhost:9000/api/items

> http://localhost:9000/resources/items

> http://localhost:9000/admin/items

> http://localhost:9000/db/items


Dependiendo tanto del usuario con el que se hace login como de la URL a la que se accede tendremos distintos resultados (200, 401, 402, 404...).


**CURL**

user/password:

`curl -v --user user:password http://localhost:9000/api/items`

`curl -v -H "Authorization: Basic dXNlcjpwYXNzd29yZA==" http://localhost:9000/api/items`


Get base65 code for basic auth:

``echo `echo user:password | base64` `` --> dXNlcjpwYXNzd29yZAo=

Get base65 decode:

``echo `echo dXNlcjpwYXNzd29yZAo= | base64 --decode` `` --> user:password






