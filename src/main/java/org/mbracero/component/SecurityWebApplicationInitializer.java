package org.mbracero.component;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * We will load the springSecurityFilterChain automatically.
 * 
 * 
 * 	Interface to be implemented in Servlet 3.0+ environments in order to configure the ServletContext
 * programmatically -- as opposed to (or possibly in conjunction with) the traditional web.xml-based approach.
 * 	Implementations of this SPI will be detected automatically by SpringServletContainerInitializer, which itself
 * is bootstrapped automatically by any Servlet 3.0 container. See its Javadoc (http://docs.spring.io/spring-framework/docs/3.2.3.RELEASE/javadoc-api/org/springframework/web/SpringServletContainerInitializer.html)
 * for details on this bootstrapping mechanism.
 * 
 * http://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/mvc.html#mvc-container-config
 */
public class SecurityWebApplicationInitializer
      extends AbstractSecurityWebApplicationInitializer {

}
