package org.mbracero.controller;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.mbracero.model.Item;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ItemController {
	@SuppressWarnings("serial")
	private List<Item> items = new CopyOnWriteArrayList<Item>() {{
		add(new Item(1l, "item 1"));
		add(new Item(2l, "item 2"));
		add(new Item(3l, "item 3"));
		add(new Item(4l, "item 4"));
		add(new Item(5l, "item 5"));
		add(new Item(6l, "item 6"));
	}};
	
	@RequestMapping(value="/api/items", method=RequestMethod.GET)
	public @ResponseBody Collection<Item> getItemsDefault() {
		// The currently authenticated user could be accessed using the following code...
		Authentication authentication =
			      SecurityContextHolder.getContext().getAuthentication();
	    User user = (User) (authentication == null ? null : authentication.getPrincipal() );
	    
	    System.out.println("-------------- Get current user by code ------------------------");
	    System.out.println("User :: " + user.getUsername());
	    System.out.println("Roles :: ");
	    Collection<GrantedAuthority> grantedAuthorityList = user.getAuthorities();
	    for (GrantedAuthority grantedAuthority : grantedAuthorityList) {
	    	System.out.println(grantedAuthority.getAuthority());
		}
	    
		return items;
	}
	
	@RequestMapping(value="/api/items2", method=RequestMethod.GET)
	public @ResponseBody Collection<Item> getItemsDefault2(@AuthenticationPrincipal User user) {
		// ... or by annotations
	    
		System.out.println("-------------- Get current user by Annotation ------------------------");
	    System.out.println("User :: " + user.getUsername());
	    System.out.println("Roles :: ");
	    Collection<GrantedAuthority> grantedAuthorityList = user.getAuthorities();
	    for (GrantedAuthority grantedAuthority : grantedAuthorityList) {
	    	System.out.println(grantedAuthority.getAuthority());
		}
	    
		return items;
	}
	
	@RequestMapping(value="/resources/items", method=RequestMethod.GET)
	public @ResponseBody Collection<Item> getItemsResources() {
		return items;
	}
	
	@RequestMapping(value="/admin/items", method=RequestMethod.GET)
	public @ResponseBody Collection<Item> getItemsAdmin() {
		return items;
	}
	
	@RequestMapping(value="/db/items", method=RequestMethod.GET)
	public @ResponseBody Collection<Item> getItemsDb() {
		return items;
	}
	
	@RequestMapping(value="/secure/items", method=RequestMethod.GET)
	public @ResponseBody Collection<Item> getItemsSecure() {
		return items;
	}
}
