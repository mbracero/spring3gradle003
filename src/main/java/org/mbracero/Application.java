package org.mbracero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

//This annotation tells Spring to auto-wire your application
@EnableAutoConfiguration
//This annotation tells Spring to look for controllers, etc.
@ComponentScan(value="org.mbracero.controller")
//This annotation tells Spring that this class contains configuration
//information
//for the application.
@Configuration


@EnableWebMvcSecurity
/**
 * Require authentication to every URL in your application
 * Generate a login form for you
 * Allow the user with the Username user and the Password password to authenticate with form based authentication
 * Allow the user to logout
 * (...)
 * See: http://docs.spring.io/spring-security/site/docs/3.2.7.RELEASE/reference/htmlsingle/#hello-web-security-java-configuration
 */
public class Application extends WebSecurityConfigurerAdapter {
	public static void main(String[] args) {
		// This call tells spring to launch the application and
		// use the configuration specified in LocalApplication to
		// configure the application's components.
		SpringApplication.run(Application.class, args);
	}
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER")
                .and()
                .withUser("admin").password("password").roles("USER", "ADMIN")
                .and()
                .withUser("dba").password("password").roles("DBA", "ADMIN");
    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/**
		 * DEFAULT CONFIGURATION:
			    http
			        .authorizeRequests()
			            .anyRequest().authenticated()
			            .and()
			        .formLogin()
			            .and()
			        .httpBasic();
        **/
		
		/**
		 * MULTIPLE HTTP SECURITY:
		 * http://docs.spring.io/spring-security/site/docs/3.2.7.RELEASE/reference/htmlsingle/#multiple-httpsecurity
		 * We can configure multiple HttpSecurity instances just as we can have multiple <http> blocks. The key is to extend the WebSecurityConfigurationAdapter multiple times.
		 */
		
		http
		// There are multiple children to the http.authorizeRequests() method each matcher is considered in the order they were declared.
        .authorizeRequests()
        	// We specified multiple URL patterns that any user can access. Specifically, any user can access a request if the URL starts with "/resources/", equals "/signup", or equals "/about".
            .antMatchers("/resources/**", "/secure/**", "/signup", "/about").permitAll()
            // Any URL that starts with "/admin/" will be resticted to users who have the role "ROLE_ADMIN". You will notice that since we are invoking the hasRole method we do not need to specify the "ROLE_" prefix.
            .antMatchers("/admin/**").hasRole("ADMIN")
            // Any URL that starts with "/db/" requires the user to have both "ROLE_ADMIN" and "ROLE_DBA"
            .antMatchers("/db/**").access("hasRole('ROLE_ADMIN') and hasRole('ROLE_DBA')")
            // Any URL that has not already been matched on only requires that the user be authenticated
            .anyRequest().authenticated()
            .and()
        .formLogin()
    		.and()
	    .requiresChannel()
	    	// https
	    	.antMatchers("/secure/**").requiresSecure()
	        .and()
	    .httpBasic();
	}
}
